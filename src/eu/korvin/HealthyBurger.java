package eu.korvin;

/**
 * Project: Bill's Burgers
 * Created by Korvin F. Ezüst on 2018-04-05
 */
class HealthyBurger extends Hamburger {
    private String addition5 = "";
    private double addition5Price = 0;
    private String addition6 = "";
    private double addition6Price = 0;

    /**
     * Constructor, make a new "healthy burger"
     *
     * @param name     name of burger
     * @param meat     type of meat
     * @param price    price of burger
     * @param currency currency used
     */
    HealthyBurger(String name, String meat, double price, String currency) {
        super(name, "brown rye", meat, price, currency);
    }

    /**
     * Add additional ingredients to the burger
     *
     * @param add1 first ingredient, name and price in an Addition object, or null
     * @param add2 second ingredient, name and price in an Addition object, or null
     * @param add3 third ingredient, name and price in an Addition object, or null
     * @param add4 fourth ingredient, name and price in an Addition object, or null
     * @param add5 fifth ingredient, name and price in an Addition object, or null
     * @param add6 sixth ingredient, name and price in an Addition object, or null
     */
    void additions(Addition add1, Addition add2, Addition add3, Addition add4, Addition add5, Addition add6) {
        super.additions(add1, add2, add3, add4);
        if (add5 != null) {
            addition5 = add5.getName();
            price += add5.getPrice();
            addition5Price = add5.getPrice();
        }
        if (add6 != null) {
            addition6 = add6.getName();
            price += add6.getPrice();
            addition6Price = add6.getPrice();
        }
    }

    @Override
    double getPrice() {
        return price;
    }

    @SuppressWarnings("EmptyMethod")
    @Override
    double getBasePrice() {
        return super.getBasePrice();
    }

    /**
     * Print the summary about the burger
     */
    void summary() {
        String add1 = "";
        String add2 = "";
        String add3 = "";
        String add4 = "";
        String add5 = "";
        String add6 = "";

        if (!getAddition1().equals(""))
            add1 = makeAddition(getAddition1(), getAddition1Price(), getCurrency());
        if (!getAddition2().equals(""))
            add2 = makeAddition(getAddition2(), getAddition2Price(), getCurrency());
        if (!getAddition3().equals(""))
            add3 = makeAddition(getAddition3(), getAddition3Price(), getCurrency());
        if (!getAddition4().equals(""))
            add4 = makeAddition(getAddition4(), getAddition4Price(), getCurrency());
        if (!addition5.equals(""))
            add5 = makeAddition(addition5, addition5Price, getCurrency());
        if (!addition6.equals(""))
            add6 = makeAddition(addition6, addition6Price, getCurrency());

        System.out.println(makeSummary(
                getName(),
                getBreadRoll(),
                getMeat(),
                getBasePrice(),
                price,
                getCurrency(),
                add1,
                add2,
                add3,
                add4,
                add5,
                add6));
    }
}
