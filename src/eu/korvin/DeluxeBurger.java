package eu.korvin;

/**
 * Project: Bill's Burgers
 * Created by Korvin F. Ezüst on 2018-04-05
 */
class DeluxeBurger extends Hamburger {
    private final String chips;
    private final double chipsPrice;
    private final String drink;
    private final double drinkPrice;

    /**
     * Constructor, create a new "deluxe burger"
     *
     * @param name       name of burger
     * @param breadRoll  type of bread roll
     * @param meat       type of meat
     * @param price      price of burger
     * @param currency   currency used
     * @param chips      type of chips/fries
     * @param chipsPrice price of chips/fries
     * @param drink      name of drink
     * @param drinkPrice price of drink
     */
    DeluxeBurger(
            String name,
            String breadRoll,
            String meat,
            double price,
            String currency,
            String chips,
            double chipsPrice,
            String drink,
            double drinkPrice) {
        super(name, breadRoll, meat, price, currency);
        this.chips = chips;
        this.chipsPrice = chipsPrice;
        this.drink = drink;
        this.drinkPrice = drinkPrice;
        super.price += chipsPrice + drinkPrice;
    }

    /**
     * No additional ingredients to this burger type, making this method empty
     */
    @SuppressWarnings({"unused", "EmptyMethod"})
    void additions() {
        // do nothing
    }

    /**
     * Print the summary about the burger
     */
    void summary() {
        String add1 = makeAddition(chips, chipsPrice, getCurrency());
        String add2 = makeAddition(drink, drinkPrice, getCurrency());

        System.out.println(makeSummary(
                getName(),
                getBreadRoll(),
                getMeat(),
                getBasePrice(),
                getPrice(),
                getCurrency(),
                add1,
                add2));
    }
}
