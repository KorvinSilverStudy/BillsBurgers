package eu.korvin;

/**
 * Project: Bill's Burgers
 * Created by Korvin F. Ezüst on 2018-04-05
 */
public class Main {
    public static void main(String[] args) {
        Hamburger hamburger = new Hamburger("Base Burger", "normal", "chicken", 4.0, "EUR");
        hamburger.additions(
                new Addition("lettuce", 0.5),
                new Addition("pickles", 1.4),
                new Addition("tomato", 1),
                new Addition("pepper", 1));
        hamburger.summary();

        hamburger = new Hamburger("Base Burger", "normal", "beef", 4.5, "EUR");
        hamburger.additions(new Addition("lettuce", 0.5), null, null, new Addition("tomato", 1));
        hamburger.summary();

        HealthyBurger healthyBurger = new HealthyBurger("Healthy Burger", "chicken", 7, "EUR");
        healthyBurger.additions(
                new Addition("pickles", 1.4),
                new Addition("tomato", 1),
                new Addition("egg", 1.8),
                new Addition("green pepper", 1),
                new Addition("red pepper", 1.2),
                new Addition("lettuce", 0.5));
        healthyBurger.summary();

        healthyBurger = new HealthyBurger("Healthy Burger", "chicken", 7, "EUR");
        healthyBurger.additions(
                null,
                new Addition("tomato", 1),
                new Addition("egg", 1.8),
                null,
                null,
                new Addition("lettuce", 0.5));
        healthyBurger.summary();

        DeluxeBurger deluxeBurger = new DeluxeBurger(
                "Deluxe Burger",
                "normal",
                "beef",
                4.5,
                "EUR",
                "french fries",
                3,
                "Pepsi",
                3);
        deluxeBurger.summary();
    }
}
