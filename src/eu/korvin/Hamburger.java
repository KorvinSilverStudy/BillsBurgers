package eu.korvin;

/**
 * Project: Bill's Burgers
 * Created by Korvin F. Ezüst on 2018-04-05
 *
 * Burger base class
 */
class Hamburger {
    private final String name;
    private final String breadRoll;
    private final String meat;
    private final double basePrice;
    double price;
    private String addition1 = "";
    private double addition1Price = 0;
    private String addition2 = "";
    private double addition2Price = 0;
    private String addition3 = "";
    private double addition3Price = 0;
    private String addition4 = "";
    private double addition4Price = 0;
    private final String currency;

    /**
     * Constructor, create a new hamburger
     *
     * @param name      name of burger
     * @param breadRoll type of bread roll
     * @param meat      type of meat
     * @param price     price of burger
     * @param currency  currency used
     */
    Hamburger(String name, String breadRoll, String meat, double price, String currency) {
        this.name = name;
        this.breadRoll = breadRoll;
        this.meat = meat;
        basePrice = price;
        this.price = price;
        this.currency = currency;
    }

    /**
     * Add additional ingredients to the hamburger
     *
     * @param add1 first ingredient, name and price in an Addition object, or null
     * @param add2 second ingredient, name and price in an Addition object, or null
     * @param add3 third ingredient, name and price in an Addition object, or null
     * @param add4 fourth ingredient, name and price in an Addition object, or null
     */
    void additions(Addition add1, Addition add2, Addition add3, Addition add4) {
        if (add1 != null) {
            addition1 = add1.getName();
            price += add1.getPrice();
            addition1Price = add1.getPrice();
        }
        if (add2 != null) {
            addition2 = add2.getName();
            price += add2.getPrice();
            addition2Price = add2.getPrice();
        }
        if (add3 != null) {
            addition3 = add3.getName();
            price += add3.getPrice();
            addition3Price = add3.getPrice();
        }
        if (add4 != null) {
            addition4 = add4.getName();
            price += add4.getPrice();
            addition4Price = add4.getPrice();
        }

    }

    String getName() {
        return name;
    }

    String getBreadRoll() {
        return breadRoll;
    }

    String getMeat() {
        return meat;
    }

    double getBasePrice() {
        return basePrice;
    }

    double getPrice() {
        return price;
    }

    String getAddition1() {
        return addition1;
    }

    double getAddition1Price() {
        return addition1Price;
    }

    String getAddition2() {
        return addition2;
    }

    double getAddition2Price() {
        return addition2Price;
    }

    String getAddition3() {
        return addition3;
    }

    double getAddition3Price() {
        return addition3Price;
    }

    String getAddition4() {
        return addition4;
    }

    double getAddition4Price() {
        return addition4Price;
    }

    String getCurrency() {
        return currency;
    }

    /**
     * Creates a summary that has all the information about the burger
     *
     * @param name      name of burger
     * @param breadRoll bread roll type
     * @param meat      meat type
     * @param basePrice base price of burger without additional ingredients
     * @param price     total price of burger with additional ingredients
     * @param currency  currency used
     * @param additions list of additional ingredients
     * @return formatted string
     */
    String makeSummary(
            String name,
            String breadRoll,
            String meat,
            double basePrice,
            double price,
            String currency,
            String... additions) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format(
                "%s (%s, %s, %.2f %s)\nAdditions:\n",
                name,
                breadRoll,
                meat,
                basePrice,
                currency));
        for (String addition : additions)
            if (!addition.equals(""))
                stringBuilder.append(addition);

        stringBuilder.append(String.format("Total price: %.2f %s\n", price, currency));

        return stringBuilder.toString();
    }

    /**
     * Create a string to be used by the makeSummary() method for additional ingredients
     *
     * @param addition an additional ingredient
     * @param price    price of ingredient
     * @param currency currency used
     * @return formatted string
     */
    String makeAddition(String addition, double price, String currency) {
        return String.format("- %s (%.2f %s)\n", addition, price, currency);
    }

    /**
     * Print the summary about the burger
     */
    void summary() {
        String add1 = "";
        String add2 = "";
        String add3 = "";
        String add4 = "";

        if (!addition1.equals(""))
            add1 = makeAddition(addition1, addition1Price, getCurrency());
        if (!addition2.equals(""))
            add2 = makeAddition(addition2, addition2Price, getCurrency());
        if (!addition3.equals(""))
            add3 = makeAddition(addition3, addition3Price, getCurrency());
        if (!addition4.equals(""))
            add4 = makeAddition(addition4, addition4Price, getCurrency());

        System.out.println(makeSummary(
                getName(),
                getBreadRoll(),
                getMeat(),
                getBasePrice(),
                getPrice(),
                getCurrency(),
                add1,
                add2,
                add3,
                add4));
    }
}
