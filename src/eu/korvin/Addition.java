package eu.korvin;

/**
 * Project: Bill's Burgers
 * Created by Korvin F. Ezüst on 2018-04-05
 *
 * Class to hold together an additional ingredient and its price
 */
class Addition {
    private final String name;
    private final double price;

    /**
     * Constructor
     *
     * @param name  name of ingredient
     * @param price price of ingredient
     */
    Addition(String name, double price) {
        this.name = name;
        this.price = price;
    }

    String getName() {
        return name;
    }

    double getPrice() {
        return price;
    }
}
